using System;
using System.ComponentModel.DataAnnotations;
namespace universitas.Models
{
    public class CollegeMajor
    {
         public  long Id {get;set;}
        public long Major_id {get; set;}
        public long College_id {get;set;}
        public int Intake {get;set;}
        [DataType(DataType.Date)]
        public DateTime? Created_on {get;set;}
        public string Created_by {get;set;}
        [DataType(DataType.Date)]
        public DateTime? Modify_on {get;set;}
        public string Modify_by {get;set;}
        [DataType(DataType.Date)]
        public DateTime? Deleted_on {get;set;}
        public string Deleted_by {get;set;}
        public bool Is_deleted {get;set;}
    }
}