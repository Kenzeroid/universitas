using System;
using System.ComponentModel.DataAnnotations;

namespace universitas.Models
{
    public class College
    {
        public long Id {get; set;}

        [StringLength(50)]
        public string Name {get; set;}

        public string State {get; set;}

        public string Accredition {get; set;}

        public double Fees {get; set;}

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }
        
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public long? deletedBy { get; set; }

        public DateTime? deletedOn { get; set; }

        public bool is_deleted { get; set; }
    }
}