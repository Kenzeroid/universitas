using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace universitas.Models
{
    public class Major
    {
        //[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        
        [Required, StringLength(20)]
        public string Major_Name { get; set; }
        public DateTime Created_On { get; set; }
        public string Created_by { get; set; }
        public DateTime? Modify_on { get; set; }
        public string Modify_by { get; set; }
        public DateTime? Deleted_on { get; set; }
        public string Deleted_by { get; set; }
        public Boolean Is_delete { get; set; }        
    }
}