using System;
using System.ComponentModel.DataAnnotations;

namespace Universitas.Models
{
    public class Student
    {
        public long Id { get; set; }
        public long Gpa { get; set; }
        [DataType(DataType.Date)]
        public DateTime Dob { get; set; }
        public string Name { get; set; }
        public string Created_by { get; set; }
        public DateTime Created_on { get; set; }
        public string Modify_by { get; set; }
        public DateTime? Modify_on { get; set; }
        public string Deleted_by { get; set; }
        public DateTime? Deleted_on { get; set; }
        public bool IsDelete { get; set; }
    }
}