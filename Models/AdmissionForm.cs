using System;
using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.schema;

namespace universitas.Models
{
    public class AdmissionForm
    {
        public long Id { get; set; }
        public long CollegeMajor { get; set; }
        public long Student { get; set; }
        public int Status { get; set; }
        public int Year { get; set; }
        public bool IsDelete {get; set;}

        [DataType(DataType.Date)]
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }

        [DataType(DataType.Date)]
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }

        [DataType(DataType.Date)]
        public DateTime? DeletedOn { get; set; }
        public string DeletedBy { get; set; }

    }
}