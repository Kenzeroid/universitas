﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Universitas.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AdmissionForm",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CollegeMajor = table.Column<long>(nullable: false),
                    Student = table.Column<long>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    Year = table.Column<int>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    DeletedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdmissionForm", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "College",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    State = table.Column<string>(nullable: true),
                    Accredition = table.Column<string>(nullable: true),
                    Fees = table.Column<double>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    deletedBy = table.Column<long>(nullable: true),
                    deletedOn = table.Column<DateTime>(nullable: true),
                    is_deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_College", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CollegeMajor",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Major_id = table.Column<long>(nullable: false),
                    College_id = table.Column<long>(nullable: false),
                    Intake = table.Column<int>(nullable: false),
                    Created_on = table.Column<DateTime>(nullable: true),
                    Created_by = table.Column<string>(nullable: true),
                    Modify_on = table.Column<DateTime>(nullable: true),
                    Modify_by = table.Column<string>(nullable: true),
                    Deleted_on = table.Column<DateTime>(nullable: true),
                    Deleted_by = table.Column<string>(nullable: true),
                    Is_deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CollegeMajor", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Major",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Major_Name = table.Column<string>(maxLength: 20, nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Created_by = table.Column<string>(nullable: true),
                    Modify_on = table.Column<DateTime>(nullable: true),
                    Modify_by = table.Column<string>(nullable: true),
                    Deleted_on = table.Column<DateTime>(nullable: true),
                    Deleted_by = table.Column<string>(nullable: true),
                    Is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Major", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Student",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Gpa = table.Column<long>(nullable: false),
                    Dob = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Created_by = table.Column<string>(nullable: true),
                    Created_on = table.Column<DateTime>(nullable: false),
                    Modify_by = table.Column<string>(nullable: true),
                    Modify_on = table.Column<DateTime>(nullable: true),
                    Deleted_by = table.Column<string>(nullable: true),
                    Deleted_on = table.Column<DateTime>(nullable: true),
                    IsDelete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Student", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdmissionForm");

            migrationBuilder.DropTable(
                name: "College");

            migrationBuilder.DropTable(
                name: "CollegeMajor");

            migrationBuilder.DropTable(
                name: "Major");

            migrationBuilder.DropTable(
                name: "Student");
        }
    }
}
