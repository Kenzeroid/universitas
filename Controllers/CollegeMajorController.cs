using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using universitas.Models;

namespace Universitas.Controllers
{
    public class CollegeMajorController : Controller
    {
        private readonly Context _context;

        public CollegeMajorController(Context context)
        {
            _context = context;
        }

        // GET: CollegeMajor
        public async Task<IActionResult> Index()
        {
            return View(await _context.CollegeMajor.ToListAsync());
        }

        // GET: CollegeMajor/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var collegeMajor = await _context.CollegeMajor
                .FirstOrDefaultAsync(m => m.Id == id);
            if (collegeMajor == null)
            {
                return NotFound();
            }

            return View(collegeMajor);
        }

        // GET: CollegeMajor/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CollegeMajor/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Major_id,College_id,Intake,Created_on,Created_by,Modify_on,Modify_by,Deleted_on,Deleted_by,Is_deleted")] CollegeMajor collegeMajor)
        {
            if (ModelState.IsValid)
            {
                _context.Add(collegeMajor);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(collegeMajor);
        }

        // GET: CollegeMajor/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var collegeMajor = await _context.CollegeMajor.FindAsync(id);
            if (collegeMajor == null)
            {
                return NotFound();
            }
            return View(collegeMajor);
        }

        // POST: CollegeMajor/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Id,Major_id,College_id,Intake,Created_on,Created_by,Modify_on,Modify_by,Deleted_on,Deleted_by,Is_deleted")] CollegeMajor collegeMajor)
        {
            if (id != collegeMajor.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(collegeMajor);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CollegeMajorExists(collegeMajor.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(collegeMajor);
        }

        // GET: CollegeMajor/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var collegeMajor = await _context.CollegeMajor
                .FirstOrDefaultAsync(m => m.Id == id);
            if (collegeMajor == null)
            {
                return NotFound();
            }

            return View(collegeMajor);
        }

        // POST: CollegeMajor/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var collegeMajor = await _context.CollegeMajor.FindAsync(id);
            _context.CollegeMajor.Remove(collegeMajor);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CollegeMajorExists(long id)
        {
            return _context.CollegeMajor.Any(e => e.Id == id);
        }
    }
}
