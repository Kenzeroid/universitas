using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using universitas.Models;

namespace Universitas.Controllers
{
    public class AdmissionFormController : Controller
    {
        private readonly Context _context;

        public AdmissionFormController(Context context)
        {
            _context = context;
        }

        // GET: AdmissionForm
        public async Task<IActionResult> Index()
        {
            return View(await _context.AdmissionForm.ToListAsync());
        }

        // GET: AdmissionForm/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var admissionForm = await _context.AdmissionForm
                .FirstOrDefaultAsync(m => m.Id == id);
            if (admissionForm == null)
            {
                return NotFound();
            }

            return View(admissionForm);
        }

        // GET: AdmissionForm/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: AdmissionForm/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,CollegeMajor,Student,Status,Year")] AdmissionForm admissionForm)
        {
            admissionForm.IsDelete = false;
            admissionForm.CreatedOn = DateTime.Now.Date;
            admissionForm.CreatedBy = "mei";
            if (ModelState.IsValid)
            {
                _context.Add(admissionForm);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(admissionForm);
        }

        // GET: AdmissionForm/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var admissionForm = await _context.AdmissionForm.FindAsync(id);
            if (admissionForm == null)
            {
                return NotFound();
            }
            return View(admissionForm);
        }

        // POST: AdmissionForm/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Id,CollegeMajor,Student,Status,Year,IsDelete,CreatedOn,CreatedBy,ModifiedOn,ModifiedBy")] AdmissionForm admissionForm)
        {
            if (id != admissionForm.Id)
            {
                return NotFound();
            }

            
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(admissionForm);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AdmissionFormExists(admissionForm.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(admissionForm);
        }

        // GET: AdmissionForm/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var admissionForm = await _context.AdmissionForm
                .FirstOrDefaultAsync(m => m.Id == id);
            if (admissionForm == null)
            {
                return NotFound();
            }

            return View(admissionForm);
        }

        // POST: AdmissionForm/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var admissionForm = await _context.AdmissionForm.FindAsync(id);
            _context.AdmissionForm.Remove(admissionForm);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AdmissionFormExists(long id)
        {
            return _context.AdmissionForm.Any(e => e.Id == id);
        }
    }
}
