using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using universitas.Models;

namespace Universitas.Controllers
{
    public class CollegeController : Controller
    {
        private readonly Context _context;

        public CollegeController(Context context)
        {
            _context = context;
        }

        // GET: College
        public async Task<IActionResult> Index(string searchString)
        {            
            var college = from c in _context.College
                        select c;
            
            if (!String.IsNullOrEmpty(searchString))
            {
                college = college.Where(s => s.Name.ToUpper().Contains(searchString.ToUpper()));
            }
            return View(await college.ToListAsync());
        }

        // GET: College/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var college = await _context.College
                .FirstOrDefaultAsync(m => m.Id == id);
            if (college == null)
            {
                return NotFound();
            }

            return View(college);
        }

        // GET: College/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: College/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,State,Accredition,Fees,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,deletedBy,deletedOn,is_deleted")] College college)
        {
            if (ModelState.IsValid)
            {
                college.CreatedBy = "Ami";
                college.CreatedOn = DateTime.Now;
                _context.Add(college);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(college);
        }

        // GET: College/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var college = await _context.College.FindAsync(id);
            if (college == null)
            {
                return NotFound();
            }
            return View(college);
        }

        // POST: College/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Id,Name,State,Accredition,Fees,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,deletedBy,deletedOn,is_deleted")] College college)
        {
            if (id != college.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    college.ModifiedBy = "Ami";
                    college.ModifiedOn = DateTime.Now;
                    _context.Update(college);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CollegeExists(college.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(college);
        }

        // GET: College/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var college = await _context.College
                .FirstOrDefaultAsync(m => m.Id == id);
            if (college == null)
            {
                return NotFound();
            }

            return View(college);
        }

        // POST: College/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            
            var college = await _context.College.FindAsync(id);
            college.deletedBy = 1;
            college.deletedOn = DateTime.Now;
            college.is_deleted = false;
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CollegeExists(long id)
        {
            return _context.College.Any(e => e.Id == id);
        }
    }
}
