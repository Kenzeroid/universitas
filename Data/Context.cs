using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using universitas.Models;

    public class Context : DbContext
    {
        public Context (DbContextOptions<Context> options)
            : base(options)
        {
        }

        public DbSet<universitas.Models.CollegeMajor> CollegeMajor { get; set; }

        public DbSet<universitas.Models.Major> Major { get; set; }
        public DbSet<Universitas.Models.Student> Student { get; set; }
        public DbSet<universitas.Models.AdmissionForm> AdmissionForm { get; set; }
        public DbSet<universitas.Models.College> College { get; set; }


        
    }
